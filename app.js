const baseAPI = "https://jsonplaceholder.typicode.com/";
const port = process.env.PORT || 3000;
const fetch = require("node-fetch");
const express = require("express");
const app = express();
const swaggerJsDoc = require("swagger-jsdoc");
const swaggerUi = require("swagger-ui-express");
  
const swaggerOptions = {
  swaggerDefinition: {
    info: {
      title: "JSONPlaceholder API",
      description: "API REST that consumes data from JSONPlaceholder",
      contact: {
        name: "Rafael Bastos"
      },
      servers: ["http://localhost:3000"]
    }
  },
  apis: ["app.js"]
};

const swaggerDocs = swaggerJsDoc(swaggerOptions);
app.use("/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerDocs));

function sendResult(req, res, data) {
  if (!data) {
    res.status(404).send("The post with de given ID was not found");
  } 
  else {
    if (req.query.sortBy && req.query.order){
      res.status(200).send(data.sort(sortResults(data, req.query.sortBy, req.query.order === 'desc' ? -1 : 1)));
    }
    else
      res.status(200).send(data);
  }
}


function sortResults(data, atribute, order) {
  data.sort(function (a, b) {
      if (a[atribute] < b[atribute]) return -order;
      else if (a[atribute] > b[atribute]) return order;
      else return 0;
  });
}


function  sendPaginatedResult(req, res, data) {
  
  let page = parseInt(req.query.page);
  let resultsPerPage = parseInt(req.query.results);
  const sort  = {}
  
  if (!resultsPerPage) {
    resultsPerPage = 10;
  }
  if (resultsPerPage < 1) {
    resultsPerPage = 10;
  }
  const pageCount = Math.ceil(data.length / resultsPerPage);
  
  if (!page) {
    page = 1;
  }
  if (page > pageCount) {
    res.status(400).send("This page does'not exists");
  }
  
  if (req.query.sortBy && req.query.order){
    data = data.sort(sortResults(data, req.query.sortBy, req.query.order === 'desc' ? -1 : 1));
  }

  res.status(200).json({
    page: page,
    pageCount: pageCount,
    data: data.slice(
      page * resultsPerPage - resultsPerPage,
      page * resultsPerPage
    )
  })
}

/**
 * @swagger
 * /api/posts:
 *    get:
 *      description: Use to return all posts
 *    parameters:
 *      - name: page
 *        in: query
 *        description: number of the page
 *        required: false
 *        schema:
 *          type: integer
 *          format: int64
 *      - name: results
 *        in: query
 *        description: number of results per page
 *        required: false
 *        schema:
 *          type: integer
 *          format: int64
 *      - name: sortBy
 *        in: query
 *        description: atribute user to sort
 *        required: false
 *        type: array
 *        items:
 *          type: string
 *          enum:
 *            - userId
 *            - id
 *            - title
 *            - body
 *      - name: order
 *        in: query
 *        description: sort order
 *        required: false
 *        type: array
 *        items:
 *          type: string
 *          enum:
 *          - asc
 *          - desc
 *          default: asc
 *    responses:
 *      '200':
 *        description: Successfully get posts
 *      '400':
 *        description: The required page was not found
 */
app.get("/api/posts/", async function(req, res) {
  fetch(`${baseAPI}posts`)
    .then(response => response.json())
    .then(data => sendPaginatedResult(req, res, data));
});


/**
 * @swagger
 * /api/posts/{postId}:
 *    get:
 *      description: Use to return data from a single
 *    parameters:
 *      - name: postId
 *        in: path
 *        description: Id of the post
 *        required: true
 *        type: string
 *        format: string
 *    responses:
 *      '200':
 *        description: Successfully created user
 *      '404':
 *        description: The specified post was not found
 */
app.get("/api/posts/:id", async function(req, res) {
  fetch(`${baseAPI}posts`)
    .then(response => response.json())
    .then(json =>
      sendResult(
        req,
        res,
        json.find(p => p.id === parseInt(req.params.id))
      )
    );
});

/**
 * @swagger
 * /api/posts/{postId}/comments:
 *    get:
 *      description: Use to return data from a single
 *    parameters:
 *      - name: postId
 *        in: path
 *        description: Id of the post
 *        required: true
 *        type: string
 *        format: string
 *      - name: sortBy
 *        in: query
 *        description: atribute user to sort
 *        required: false
 *        type: array
 *        items:
 *          type: string
 *          enum:
 *            - id
 *            - name
 *            - email
 *            - body
 *      - name: order
 *        in: query
 *        description: sort order
 *        required: false
 *        type: array
 *        items:
 *          type: string
 *          enum:
 *          - asc
 *          - desc
 *          default: asc
 *    responses:
 *      '200':
 *        description: Successfully created user
 *      '404':
 *        description: The specified post was not found
 */
app.get("/api/posts/:id/comments", async function(req, res) {
  fetch(`${baseAPI}comments`)
    .then(response => response.json())
    .then(json =>
      sendResult(
        req,
        res,
        json.filter(c => c.postId === parseInt(req.params.id))
      )
    );
});

app.listen(port, () => console.log(`Listening on port ${port}...`));
